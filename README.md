# Przykładowa Tabela

| Kolumna 1 | Kolumna 2 | Kolumna 3 |
|:---------:|:---------:|:---------:|
|   Wartość A  |   Wartość B  |   Wartość C  |
|   Wartość X  |   Wartość Y  |   Wartość Z  |

```python
# Przykładowy kod Python z podkreśleniem składni
def przykladowa_funkcja():
    print("Hello, World!")
    return 42

[^1]: To jest przykładowa stopka.

## Oznaczone nagłówki
### Nagłówek 1
### Nagłówek 2
### Nagłówek 3

## Lista Zadań
- [x] Zadanie 1
- [ ] Zadanie 2
- [x] Zadanie 3

Użyto dwóch emotikonów:
:smile: :rocket:
